import React from 'react';
import './ShowInfo.css';

const ShowInfo = ({ imgHref, description, name, language, genres, premiered, status }) => {
    return (
        <div className="info-block">
            <div className="first-block">
                <img src={imgHref} alt=""/>
            </div>
            <div className="second-block">
                <h2>{name}</h2>
                <div dangerouslySetInnerHTML={{__html: description}} className="description"/>
                <ul>
                    <li>Language: {language}</li>
                    <li>Genres: {genres.join("; ")};</li>
                    <li>Premiered: {premiered}</li>
                    <li>Status: {status}</li>
                </ul>
            </div>
        </div>
    );
};

export default ShowInfo;