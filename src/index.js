import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {applyMiddleware, createStore} from 'redux';
import thunk from "redux-thunk";
import reducer from './store/reducer';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import SeriesContainer from "./Containers/Series/SeriesContainer";


const store = createStore(reducer, applyMiddleware(thunk));

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App/>
            <Switch>
                <Route path="/" exact component={SeriesContainer}/>
                <Route path="/shows/:id" exact component={SeriesContainer}/>
                <Route render={() => <h1>Not found</h1>}/>
            </Switch>
        </BrowserRouter>
    </Provider>
)

ReactDOM.render(
    app,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
