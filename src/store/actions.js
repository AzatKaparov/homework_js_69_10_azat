export const SET_CURRENT_SHOW = 'SET_CURRENT_SHOW';

export const setCurrentShow = show => ({type: SET_CURRENT_SHOW, show: show});
