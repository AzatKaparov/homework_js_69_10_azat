import {SET_CURRENT_SHOW} from "./actions";

const initialState = {
    chosenShow: null,
};

const reducer = (state=initialState, action) => {
    switch (action.type) {
        case SET_CURRENT_SHOW:
            return {
                ...state,
                chosenShow: action.show
            };
        default:
            return state;
    }
};

export default reducer;