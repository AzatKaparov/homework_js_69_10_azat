import React, {useEffect, useReducer} from 'react';
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import './SeriesContainer.css';
import axiosApi from "../../axiosApi";
import {useDispatch, useSelector} from "react-redux";
import {setCurrentShow} from "../../store/actions";
import ShowInfo from "../../Components/ShowInfo";


const SET_SEARCH_VALUE = 'SET_SEARCH_VALUE';
const SET_NEW_ARRAY = 'SET_NEW_ARRAY';

const initialState = {
    shows: [],
    searchValue: '',
};

const reducer = (state=initialState, action) => {
    switch (action.type) {
        case SET_SEARCH_VALUE:
            return {
                ...state,
                searchValue: action.newValue
            };
        case SET_NEW_ARRAY:
            return  {
                ...state,
                shows: action.newArray
            };
        default:
            return state;
    }
}


const SeriesContainer = ({history}) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const reduxDisp = useDispatch();

    const show = useSelector(state => state.chosenShow);
    if (show) {
        console.log(show);
    }

    const autocompleteChange = (e, newValue) => {
        if (newValue) {
            reduxDisp(setCurrentShow(newValue.show));
            history.push(`/shows/${newValue.show.id}`);
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get(`?q=${state.searchValue}`);
            dispatch({type: SET_NEW_ARRAY, newArray: response.data});
        }
        fetchData().catch(console.error);
    }, [state.searchValue]);

    return (
        <div className="container">
            <h1 className="main-title">Search for TV Show</h1>
            <Autocomplete
                id="combo-box"
                inputValue={state.searchValue}
                onChange={autocompleteChange}
                onInputChange={(e, newVal) => {
                    dispatch({type: SET_SEARCH_VALUE, newValue: newVal});
                }}
                options={state.shows}
                getOptionLabel={(option) => option.show.name}
                style={{ width: 300 }}
                renderInput={(params) =>
                    <TextField
                        style={{width: 1000}}
                        {...params}
                        label="Enter a show's name"
                        variant="outlined"
                    />}
            />
            {show && <ShowInfo
                imgHref={show.image.medium}
                description={show.summary}
                name={show.name}
                language={show.language}
                genres={show.genres}
                premiered={show.premiered}
                status={show.status}
            />}
        </div>
    );
};

export default SeriesContainer;